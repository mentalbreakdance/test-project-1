

let numbers = prompt('Введите два числа через пробел (5 10)'),
    operation = prompt('Введите одну математическую операцию (+, -, *, /)');

function fMath(a, b, operation) {
  a = parseFloat(a);
  b = parseFloat(b);
  if(operation === '+') return a + b;
  if(operation === '-') return a - b;
  if(operation === '*') return a * b;
  if(operation === '/') return a / b;
}

let prevNumber = numbers,
    prevOperation = operation;

numbers = numbers.split(' ');
while(1) {
  if(isNaN(+numbers[0]) || isNaN(+numbers[1])) { // + -> parseInt
    alert('Вы ввели некорректный данные. Попробуйте заново.');

    numbers = prompt('Введите два числа через пробел (5 10)', prevNumber);
    operation = prompt('Введите одну математическую операцию (+, -, *, /)', prevOperation);

    prevNumber = numbers;
    prevOperation = operation;

    numbers = numbers.split(' ');
  } else {
    console.log( fMath(numbers[0], numbers[1], operation) );
    break;
  }
}
